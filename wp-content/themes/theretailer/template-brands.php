<?php
/*
Template Name: Brands
*/

get_header(); ?>

<div class="global_content_wrapper">

<div class="container_12">

    <div class="grid_12">

				    	<?php // $term = $wp_query->queried_object; ?>
				    
				    	<h1 class="entry-title portfolio_title"><?php the_title(); ?></h1>
				  
				        <div class="content-area portfolio_section">
				        	<div class="content_wrapper">				
								
				                <div class="items_wrapper">
								
								<?php
								
$product_brands = get_terms( 'product_brand', array('hide_empty'=>false) );

foreach( $product_brands as $brand ){
	?>

<div class="portfolio_4_col_item_wrapper mix <?php echo strtolower(strip_tags(get_the_term_list( get_the_ID(), 'portfolio_filter', ""," " ))); ?>">
    <div class="portfolio_item">
        <div class="portfolio_item_img_container">

<?php
	$thumbnail_id = get_woocommerce_term_meta( $brand->term_id, 'thumbnail_id', true );
$image = wp_get_attachment_url( $thumbnail_id );
if ( $image ) {
echo '<img src="' . $image . '" alt="" />';
}
?>

        </div>
        <a href="<?php echo get_permalink(get_the_ID()); ?>"><h3><?php echo $brand->name; ?></h3></a>
        <div class="portfolio_sep"></div>
        <div class="portfolio_item_cat">

		<?php if( get_field('field_name') ): ?>
        	<a href="<?php the_field('field_name'); ?>" ><?php echo _x('Download catalog', 'theretailer'); ?></a>
        <?php endif; ?>
        
        </div>
    </div>
</div>

	<?php
} ?>
				                
				                </div>

				                <div class="clr"></div>

							</div><!-- #content .site-content -->
						</div><!-- #primary .content-area -->
				        
				        <?php theretailer_content_nav( 'nav-below' ); ?>
        
	</div>

</div>

</div>

<?php get_template_part("light_footer"); ?>
<?php get_template_part("dark_footer"); ?>

<?php get_footer(); ?>