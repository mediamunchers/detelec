<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */
 
global $theretailer_theme_options;

global $wp_query;

$archive_product_sidebar = 'no';

if ( ($theretailer_theme_options['sidebar_listing']) && ($theretailer_theme_options['sidebar_listing'] == 1) ) { $archive_product_sidebar = 'yes'; };

if (isset($_GET["product_listing_sidebar"])) { $archive_product_sidebar = $_GET["product_listing_sidebar"]; }

get_header('shop'); ?>

    <div class="global_content_wrapper">
    
    <div class="container_12">

<?php

$taxonomy = 'product_cat';

$term_id = get_queried_object()->term_id;
$termchildren = get_term_children( $term_id, $taxonomy );

if( $termchildren ){

    ?><div class="category_header">
        <h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
    </div>
    <ul class="term_grid"><?php
            
    foreach( $termchildren as $child ){

        $term = get_term_by( 'id', $child, $taxonomy );

        $thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
        $imageUrl = wp_get_attachment_image_src( $thumbnail_id, 'thumbnail' );

        $termUrl = get_term_link( $term->term_id, $taxonomy );

        ?>

        <li class="content_grid_3">
                
            <div class="image_container">
                <a href="<?php echo $termUrl; ?>">

                    <div class="loop_products_thumbnail_img_wrapper front">
                        <img src="<?php echo $imageUrl[0]; ?>" alt="" />
                    </div>

                    <div class="loop_products_additional_img_wrapper back">
                       
                    </div>
                    
                </a>
                <div class="clr"></div>
            </div>
            
            <p><a href="<?php echo $termUrl; ?>"><?php echo $term->name; ?></a></p>
            <div class="portfolio_sep"></div>

        </li>

        <?php
        
    }

    ?></ul><?php

} else {

    if( have_posts() ) :

        ?>

        <?php if ($archive_product_sidebar != "yes") { ?>            
            <div class="grid_12">    
        <?php } else { ?>
            <div class="grid_9 push_3">           
        <?php } ?>
            
            <?php if ($archive_product_sidebar != "yes") { ?>            
                <div class="listing_products_no_sidebar">           
            <?php } else { ?> 
                <div class="listing_products">    
            <?php } ?>

            <div class="category_header">

                <h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
                
                <?php
                
                woocommerce_get_template( 'loop/result-count.php' );
                
                global $woocommerce;

                $orderby = isset( $_GET['orderby'] ) ? woocommerce_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
        
                woocommerce_get_template( 'loop/orderby.php', array( 'orderby' => $orderby ) );
                
                ?>
                
                <div class="clr"></div>
        
                <div class="hr padding30 fixbottom10"></div>
            
            </div>

        <?php do_action( 'woocommerce_archive_description' ); ?>

        <?php if ( is_tax() ) : ?>
            <?php do_action( 'woocommerce_taxonomy_archive_description' ); ?>
        <?php elseif ( ! empty( $shop_page ) && is_object( $shop_page ) ) : ?>
            <?php do_action( 'woocommerce_product_archive_description', $shop_page ); ?>
        <?php endif; ?>

        <?php

        if (woocommerce_product_subcategories()) : ?>Yes<hr class="paddingbottom40" /><?php endif;
    
        woocommerce_product_loop_start();

        while( have_posts() ) : the_post() ;

            woocommerce_get_template_part( 'content', 'product' );

        endwhile;

        woocommerce_product_loop_end();

        ?>
        </div>
        </div>
        
        <?php if ($archive_product_sidebar == "yes") { ?>  
            <?php if ( is_active_sidebar( 'widgets_product_listing' ) ) : ?>
                <div class="grid_3 pull_9">

                    <div class="gbtr_aside_column_left">
                        <?php dynamic_sidebar('widgets_product_listing'); ?>
                    </div>

                    <div class="gbtr_aside_column_left widget">

                        <?php

                        $term_ids = get_posts_brands( 'product_brand' );

                        if( $term_ids ){

                            ?><ul class="cat_brands"><?php
                            ?><h4 class="widget-title"><?php _e( 'Merken.', 'theretailer' ); ?></h4><?php


                                foreach( $term_ids as $term ){

                                    $thumbnail_id = get_woocommerce_term_meta( $term, 'thumbnail_id', true ); 

                                    $term_link = get_term_link( $term, 'product_brand');

                                    $image_attributes = wp_get_attachment_image_src( $thumbnail_id, 'medium' ); // returns an array
                                    
                                    if($image_attributes){

                                        ?>
                                        <li>
                                            <a href="<?php echo $term_link; ?>"><img src="<?php echo $image_attributes[0]; ?>"></a>
                                        </li>
                                        <?php

                                    }

                                }

                            ?></ul><?php

                        }

                        ?>

                    </div>

                </div>            
            <?php endif; ?>
                      
        <?php } ?>
        <?php


    else :

        if ( ! woocommerce_product_subcategories( array( 'before' => '<ul class="products">', 'after' => '</ul>' ) ) ) :?>
    
            <p><?php _e( 'No products found which match your selection.', 'theretailer' ); ?></p><?php

        endif;

    endif;

    if ( $wp_query->max_num_pages > 1 ) {
        if (function_exists("emm_paginate")) {
            emm_paginate();
        }
    }

}

?>         
        
    </div>
    
    </div>

<?php get_template_part("light_footer"); ?>
<?php get_template_part("dark_footer"); ?>

<?php get_footer('shop'); ?>
