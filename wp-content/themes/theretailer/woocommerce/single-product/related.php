<?php
/**
 * Related Products
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

global $product, $woocommerce_loop, $theretailer_theme_options;

if ((!isset($theretailer_theme_options['related_products_on_product_page'])) || ($theretailer_theme_options['related_products_on_product_page'] == "1")) {

	$related = $product->get_related(12);
	
	if ( sizeof($related) == 0 ) return;
	
	$args = apply_filters('woocommerce_related_products_args', array(
		'post_type'				=> 'product',
		'ignore_sticky_posts'	=> 1,
		'no_found_rows' 		=> 1,
		'posts_per_page' 		=> $posts_per_page,
		'orderby' 				=> $orderby,
		'post__in' 				=> $related
	) );
	
	$products = new WP_Query( $args );
	
	$woocommerce_loop['columns'] 	= $columns;
	
	if ( $products->have_posts() ) : ?>
	
		<?php $sliderrandomid = rand() ?>
		
	<script>
	jQuery(document).ready(function($) {
		
		var featured_products_slider = $("#featured-products-<?php echo $sliderrandomid ?>");
		
		if ( $(".gbtr_items_slider_id_<?php echo $sliderrandomid ?>").parents('.wpb_column').hasClass('vc_span6') ) {
			
			featured_products_slider.owlCarousel({
				items:2,
				itemsDesktop : false,
				itemsDesktopSmall :false,
				itemsTablet: false,
				itemsMobile : false,
				lazyLoad : true,
				/*autoHeight : true,*/
		
			});
			
		} else {
		
			featured_products_slider.owlCarousel({
				items:4,
				itemsDesktop : false,
				itemsDesktopSmall :false,
				itemsTablet: [770,3],
				itemsMobile : [480,2],
				lazyLoad : true,
				/*autoHeight : true,*/
		
			});
		}
	
		$('.gbtr_items_slider_id_<?php echo $sliderrandomid ?>').on('click','.big_arrow_left',function(){ 
			featured_products_slider.trigger('owl.prev');
		})
		$('.gbtr_items_slider_id_<?php echo $sliderrandomid ?>').on('click','.big_arrow_right',function(){ 
			featured_products_slider.trigger('owl.next');
		})
	
	});
	</script>
		
		<div class="grid_12">
		
			<div class="slider-master-wrapper featured-products-wrapper gbtr_items_slider_id_<?php echo $sliderrandomid ?> <?php if ( $title=='' ) echo 'slider-without-title'?>">
    
        <div class="gbtr_items_sliders_header">
            <div class="gbtr_items_sliders_title">
                <div class="gbtr_featured_section_title"><strong><?php echo $title ?></strong></div>
            </div>
            <div class="gbtr_items_sliders_nav">                        
                <a class='big_arrow_right'></a>
                <a class='big_arrow_left'></a>
                <div class='clr'></div>
            </div>
        </div>
    
        <div class="gbtr_bold_sep"></div>   
    
        <div class="slider-wrapper">
			<div class="slider" id="featured-products-<?php echo $sliderrandomid ?>">
				<?php
		
				$args = array(
					'post_status' => 'publish',
					'post_type' => 'product',
					'ignore_sticky_posts'   => 1,
					'meta_key' => '_featured',
					'meta_value' => 'yes',
					'posts_per_page' => $per_page,
					'orderby' => $orderby,
					'order' => $order,
				);
				
				$products = new WP_Query( $args );
				
				if ( $products->have_posts() ) : ?>
							
					<?php while ( $products->have_posts() ) : $products->the_post(); ?>
				
						<?php woocommerce_get_template_part( 'content', 'product' ); ?>
			
					<?php endwhile; // end of the loop. ?>
					
				<?php
				
				endif; 
				
				?>
			</div><!--.slider-->
		</div><!--.slider-wrapper-->
    
    </div>
		
		</div>
	
	<?php endif;
	
	wp_reset_postdata();

}
