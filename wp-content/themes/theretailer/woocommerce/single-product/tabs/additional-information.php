<?php
/**
 * Additional Information tab
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce, $post, $product;

$heading = apply_filters( 'woocommerce_product_additional_information_heading', __( 'Additional Information', 'theretailer' ) );
?>

<?php $product->list_attributes(); ?>

<table class="shop_attributes" style="border-top: 1px dotted rgba(0,0,0,.1);">
	<tbody>
		<tr>
			<th>Energielabel</th>
			<td>
				<div style="position:relative;">
					<?php $energy_label = get_field('energy_label'); ?>
					<?php /* if(in_array('A++', $energy_label)) */ ?>
					<ul class="energylabels">
						<li><?php if(in_array('A++', $energy_label)){ ?><img src="<?php bloginfo('template_url'); ?>/images/aplusplus.png" alt="A++"/><? } else { ?><img src="<?php bloginfo('template_url'); ?>/images/aplusplusd.png" alt="A++"/><? } ?></li>
						<li><?php if(in_array('A+', $energy_label)){ ?><img src="<?php bloginfo('template_url'); ?>/images/aplus.png" alt="A+"/><? } else { ?><img src="<?php bloginfo('template_url'); ?>/images/aplusd.png" alt="A+"/><? } ?></li>
						<li><?php if(in_array('A', $energy_label)){ ?><img src="<?php bloginfo('template_url'); ?>/images/a.png" alt="A"/><? } else { ?><img src="<?php bloginfo('template_url'); ?>/images/ad.png" alt="A"/><? } ?></li>
						<li><?php if(in_array('B', $energy_label)){ ?><img src="<?php bloginfo('template_url'); ?>/images/b.png" alt="B"/><? } else { ?><img src="<?php bloginfo('template_url'); ?>/images/bd.png" alt="B"/><? } ?></li>
						<li><?php if(in_array('C', $energy_label)){ ?><img src="<?php bloginfo('template_url'); ?>/images/c.png" alt="C"/><? } else { ?><img src="<?php bloginfo('template_url'); ?>/images/cd.png" alt="C"/><? } ?></li>
						<li><?php if(in_array('D', $energy_label)){ ?><img src="<?php bloginfo('template_url'); ?>/images/d.png" alt="D"/><? } else { ?><img src="<?php bloginfo('template_url'); ?>/images/dd.png" alt="D"/><? } ?></li>
						<li><?php if(in_array('E', $energy_label)){ ?><img src="<?php bloginfo('template_url'); ?>/images/e.png" alt="E"/><? } else { ?><img src="<?php bloginfo('template_url'); ?>/images/ed.png" alt="E"/><? } ?></li>
						<li><?php if(in_array('F', $energy_label)){ ?><img src="<?php bloginfo('template_url'); ?>/images/f.png" alt="F"/><? } else { ?><img src="<?php bloginfo('template_url'); ?>/images/fd.png" alt="F"/><? } ?></li>
						<li><?php if(in_array('G', $energy_label)){ ?><img src="<?php bloginfo('template_url'); ?>/images/g.png" alt="G"/><? } else { ?><img src="<?php bloginfo('template_url'); ?>/images/gd.png" alt="G"/><? } ?></li>
					</ul>
				</div>
			</td>
		</tr>
	</tbody>
</table>