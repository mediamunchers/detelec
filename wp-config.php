<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'detelec');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>KAqpDde&?q3 u $ VE$I`O:h?z#MxPx^Q%V4 .l]LTY~83E@=(aCk84RhkA,/,]');
define('SECURE_AUTH_KEY',  '0p|_A_$&y%9K]_o(GGl3SGHEBueH@4@-Pf#McF{MK0X|feq.=)(;K85ZHoK^cY:2');
define('LOGGED_IN_KEY',    'vU1mY`b[+@}B2(>%<`cpzdWDH,U1HRH@+8^+t+|Yt|LD?|_^Pp[1#ij^maKQUS)y');
define('NONCE_KEY',        '<#2v4cXWNPQ;LqYOpi-1-pguu&QCEJ0qB_N:/BcFFU70fRZ 7@0N:QqW:n`0i4V$');
define('AUTH_SALT',        'aI6KW7S6/f0MFaqJ:jA@0xVGsRzp^!<5I&TL@-7!8,6p;NBtg!{k-Dj_W+0]S Vb');
define('SECURE_AUTH_SALT', '8w|sFD0HlC,qX1^qc^0u@urfEnw&=[Q>t,g/g]w_:TOyO(<AW:nJOR9Kc}+$Or6T');
define('LOGGED_IN_SALT',   '&Pbl7V.A|]T[CEX1a2VdvbeRg8_PMIJcE|4fZ!k>V4dO6-5(6DefLiodxT!]i[-M');
define('NONCE_SALT',       'L!b!&f,77S3.,(=kGB7qeI*Yh.`#nzU8e:WuXi<!kvL&I;3-VP`~DaW U!5L+:h:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
